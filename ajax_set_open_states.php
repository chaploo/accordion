<?php

/**
 * @module          Accordion
 * @author          ISB
 * @copyright       2019-2024 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

header('Content-Type: application/javascript');

$database = LEPTON_database::getInstance();

$aAllRelevantIDS = $_POST['ids'];
$ignoreItem = $_POST['item_id'];
$targetID = 0;
$newState = -1;

foreach ($aAllRelevantIDS as $nowItem)
{
    $aTemp = explode("_", $nowItem);
    $tempId = intval(array_pop($aTemp));
        
    if ($nowItem != $ignoreItem)
    {
        $database->simple_query("UPDATE ".TABLE_PREFIX."mod_accordion SET `open`=0 WHERE `id`=".$tempId);        
    } else {
        $state = $database->get_one("SELECT `open` FROM ".TABLE_PREFIX."mod_accordion WHERE `id`=".$tempId);
        $newState = (intval($state) == 0) ? 1 : 0;
        $database->simple_query("UPDATE ".TABLE_PREFIX."mod_accordion SET `open`=".$newState." WHERE `id`=".$tempId);        
        $targetID = $tempId;
    }
}

echo json_encode("Item open-state is saved. [".$targetID."] ".(['closed', 'open'][$newState])."")."\n";
