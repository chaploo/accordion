<?php

/**
 * @module          Accordion
 * @author          ISB
 * @copyright       2019-2024 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$module_directory     = 'accordion';
$module_name          = 'Accordion';
$module_function      = 'page';
$module_version       = '2.1.0';
$module_platform      = '7.x';
$module_author        = 'ISB';
$module_home          = 'https://www.internet-service-berlin.de';
$module_guid          = 'f4eb93f1-325b-40c8-99fc-40885a1daab7';
$module_license       = '<a href="https://www.gnu.org/licenses/gpl-3.0" target="_blank">GNU General Public License 3</a>';
$module_license_terms = 'no license terms';
$module_description   = 'Displays accordions using Fomantic Library, <a href="https://fomantic-ui.com/modules/accordion.html" target="_blank">see details</a>';
