<?php

/**
 * @module          Accordion
 * @author          ISB
 * @copyright       2019-2024 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

//get instance of own module class
$oAC = accordion::getInstance();
$oAC->init_section( $page_id, $section_id );


if(isset($_POST['show_info'] ))
{
	$oAC->show_info();
} 
elseif(isset($_POST['edit_accordion'] ) && $_POST['edit_accordion'] != 0 )
{
		$oAC->edit_accordion($_POST['edit_accordion']);
}
elseif(isset($_POST['save_accordion'] ) && $_POST['save_accordion'] != 0 )
{
		$oAC->save_accordion($_POST['save_accordion']);
}
elseif(isset($_POST['duplicate_accordion'] ) && $_POST['duplicate_accordion'] != 0 )
{
		$oAC->duplicate_accordion($_POST['duplicate_accordion']);
}
elseif(isset($_POST['add_accordion'] ) && $_POST['add_accordion'] != 0 )
{
		$oAC->add_accordion($_POST['add_accordion']);
}
elseif(isset($_POST['delete_accordion'] ) && $_POST['delete_accordion'] != 0 )
{
		$oAC->delete_accordion($_POST['delete_accordion']);
}
elseif(isset($_POST['move_down'] ) && $_POST['move_down'] != 0 )
{
		$oAC->move_down($_POST['move_down']);
}
elseif(isset($_POST['move_up'] ) && $_POST['move_up'] != 0 )
{
		$oAC->move_up($_POST['move_up']);
}
elseif(isset($_POST['open_accordion'] ) && $_POST['open_accordion'] != 0 )
{
		$oAC->open_accordion($_POST['open_accordion']);
}
else
{	
	$min_position = $oAC->database->get_one(" SELECT MIN(position) FROM ".TABLE_PREFIX."mod_accordion WHERE section_id = ".$section_id." ");
	$max_position =	$oAC->database->get_one(" SELECT MAX(position) FROM ".TABLE_PREFIX."mod_accordion WHERE section_id = ".$section_id." ");

	$form_values = array(
		'min_pos'		=> $min_position,
		'max_pos'		=> $max_position,
		'oAC'			=> $oAC,
		'section_id'	=> $section_id,
		'page_id'		=> $page_id,
		'read_me'		=> 'https://www.internet-service-berlin.de/webdesign/module/accordion.php',
		'leptoken'		=> get_leptoken()
		
	);
	
	 //	get the template-engine.
	$oTwig = lib_twig_box::getInstance();
	$oTwig->registerModule('accordion');
		
	echo $oTwig->render( 
		"@accordion/modify.lte",	//	template-filename
		$form_values			//	template-data
	);	
}
