<?php

/**
 * @module          Accordion
 * @author          ISB
 * @copyright       2019-2024 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
class accordion_frontend extends LEPTON_abstract_frontend
{
	public array $all_accordions = [];
	public string $action_url = LEPTON_URL.'/modules/accordion/';	
	public string $view_url = LEPTON_URL.PAGES_DIRECTORY;
		
	public LEPTON_database $database;
	static $instance;	

	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();
	}
	
	public function init_page( $iPageID = 0, $iSectionID = 0 )
	{
		$page_link = $this->database->get_one("SELECT link FROM ".TABLE_PREFIX."pages WHERE page_id = ".$iPageID);
		$this->view_url = LEPTON_URL.PAGES_DIRECTORY.$page_link.PAGE_EXTENSION;
		
		//get array of all_accordions
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_accordion WHERE section_id = ".$iSectionID." AND active = 1 ORDER BY position ASC ",
			true,
			$this->all_accordions,
			true
		);
		
		foreach($this->all_accordions as &$transform)
		{
			$content = htmlspecialchars_decode($transform['content']);

			// htmlpurifier, see: http://htmlpurifier.org/docs
			$oPURIFIER = lib_lepton::getToolInstance("htmlpurifier");
			$transform['content'] = $oPURIFIER->purify($content);	
		}	
	}		
}
