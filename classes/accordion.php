<?php

/**
 * @module          Accordion
 * @author          ISB
 * @copyright       2019-2024 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
class accordion extends LEPTON_abstract
{
	public array $all_accordions =[];
	public string $addon_color = 'olive';
	public string $action = ADMIN_URL.'/pages/modify.php?page_id=';
	public string $mod_action = LEPTON_URL.'/modules/accordion/modify.php';	
		
	public object|null $oTwig = null;
	public object|null $oLO = null;
	public LEPTON_admin $admin;
	public LEPTON_database $database;
	
	static $instance;	
	
	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();		
		$this->admin = LEPTON_admin::getInstance();
		$this->oTwig = lib_twig_box::getInstance();
		$this->oTwig->registerModule('accordion');
		$this->oLO = LEPTON_order::getInstance(TABLE_PREFIX.'mod_accordion', 'position', 'id', 'section_id');					
	}
	
	public function init_section( $iPageID = 0, $iSectionID = 0 )
	{
		// Get array of all_accordions
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_accordion WHERE page_id = ".$iPageID." AND section_id = ".$iSectionID." ORDER BY position ASC",
			true,
			$this->all_accordions,
			true
		);		
	}

	public function show_info()
	{
		$support_link = '<a href="#">NO Live-Support / FAQ</a>';	
		$readme_link = '<a href="https://www.internet-service-berlin.de/webdesign/module/accordion.php" class="extern" target="_blank">Readme</a>';
		$page_id = $_POST['show_info'];
		
		$form_values = array(
			'image'			=> "https://www.internet-service-berlin.de/media/module/accordion.gif",// 201x201px
			'oAC'			=> $this,
			'readme_link'	=> $readme_link,
			'page_id'		=> $page_id,
			'SUPPORT'		=> $support_link,	
			'leptoken'		=> get_leptoken()
			
		);

		// Get the template-engine	
		echo $this->oTwig->render( 
			"@accordion/info.lte",	//	template-filename
			$form_values			//	template-data
		);		
	}	

	public function edit_accordion(int $iAccordionId = 0 ): void
	{
        // get current accordion from database
        $current_accordion =[];
        $this->database->execute_query(
            "SELECT * FROM ".TABLE_PREFIX."mod_accordion WHERE id = ".$iAccordionId,
            true,
            $current_accordion,
            false
        );	

        //create wysiwyg : show_wysiwyg_editor($name,$id,$content,$width,$height, $prompt)
        require LEPTON_PATH.'/modules/'.WYSIWYG_EDITOR.'/include.php';
        $show_wysiwyg = show_wysiwyg_editor('content','ac_content',$current_accordion['content'],'100%','250',false);				
    
        $form_values = array(
            'oAC'				=> $this,
            'current_accordion'	=> $current_accordion,
            'show_wysiwyg'		=> $show_wysiwyg,				
            'leptoken'			=> get_leptoken()
            
        );

        // get the template-engine	
        echo $this->oTwig->render( 
            "@accordion/accordion_detail.lte",	//	template-filename
            $form_values			//	template-data
        );
        
        $this->admin->print_footer();
	} 
	
	public function save_accordion(int $iAccordionId = 0 ): void
	{
        $_POST['title'] = strip_tags($_POST['title']);
        $request = LEPTON_request::getInstance();	

        if(!isset($_POST['active']))
        {
            $_POST['active'] = 0;
        }
        
        // set section_id to return to the current section		
        if (isset($_POST['section_id'])) 
        {
            $_SESSION['last_edit_section'] = intval($_POST['section_id']);
        }
        
        $all_names = [
            'title'			=> ['type' => 'string_clean', 'default' => "", 'range' => ""],
            'content'		=> ['type' => 'string_chars', 'default' => "", 'range' => ""],
            'active'		=> ['type' => 'integer', 'default' => "1", 'range' => ""]
        ];

        $all_values = $request->testPostValues($all_names);			
        
        $this->database->build_and_execute(
            'UPDATE',
            TABLE_PREFIX."mod_accordion",
            $all_values,
            'id = '.$iAccordionId
        );	

        // save = ok and forward to start screen		
        $this->admin->print_success(
            $this->language['save_ok'],
            $this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']
        );
	}
	
	public function duplicate_accordion(int $iAccordionId = 0): void
	{
		
        // get current accordion from database
        $current_accordion =[];
        $this->database->execute_query(
            "SELECT page_id, section_id, title, content, position FROM ".TABLE_PREFIX."mod_accordion WHERE id = ".$iAccordionId,
            true,
            $current_accordion,
            false
        );	
        
        // set section_id to return to the current section		
        if (isset($_POST['section_id'])) 
        {
            $_SESSION['last_edit_section'] = $_POST['section_id'];
        }
        
        // Get new order
        $position = $this->oLO->get_new($current_accordion['section_id']);			

        // overwrite some values
        $current_accordion['title'] .= "-Kopie";
        $current_accordion['position'] = $position;
        
        // Use the current_accordion for the "clone"
        $result = $this->database->build_and_execute (
            "INSERT",
            TABLE_PREFIX."mod_accordion",
            $current_accordion
        );

        // save = ok and forward to start screen		
        $this->admin->print_success(
            $this->language['save_ok'],
            $this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']
        );
		
	}

	public function add_accordion(int $iAccordionId = 0): void
	{
		if(isset($_POST['add_accordion'] ) && $_POST['add_accordion'] != 0 )
		{
			$section_id = intval($_POST['add_accordion']);
			$page_id = intval($_POST['page_id']);
			$max_position = $this->database->get_one("SELECT MAX(position) as id FROM ".TABLE_PREFIX."mod_accordion WHERE section_id = ".$section_id);
			if ($max_position == NULL)
			{
				$position = 1;
			}
			else
			{
				$position = $max_position + 1;
			}
				
			// set section_id to return to the current section			
			if (isset($section_id)) 
			{
				$_SESSION['last_edit_section'] = intval($section_id);
			}
			
			$fields = [
				'page_id'    => $page_id,
				'section_id' => $section_id,
				'title'      => "Neuer Eintrag",
				'content'    => "Text",
				'position'   => $position
			];
			
			$result = $this->database->build_and_execute (
				"INSERT",
				TABLE_PREFIX."mod_accordion",
				$fields
			);
			
			// save = ok and forward to start screen
			$this->admin->print_success(
                $this->language['save_ok'],
                $this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']
            );			
		}
	}

	public function move_down( $iAccordionId = 0 )
	{
		if(isset($_POST['move_down'] ) && $_POST['move_down'] != 0 )
		{
			$accordion_id = intval($_POST['move_down']);
			$section_id = $_POST['section_id'];
		
			// set section_id to return to the current section			
			if (isset($section_id)) 
			{
				$_SESSION['last_edit_section'] = intval($section_id);
			}
			
			// Create new order object an reorder
			if ($this->oLO->move_down($accordion_id)) 
			{
				// Clean up ordering
				$this->oLO->clean($section_id); 
				
				// save = ok and forward to start screen
				$this->admin->print_success(
                    $this->language['save_ok'],
                    $this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']
                );			
			}
		}
	}	


	public function move_up( $iAccordionId = 0 )
	{
		if(isset($_POST['move_up'] ) && $_POST['move_up'] != 0 )
		{
			$accordion_id = intval($_POST['move_up']);
			$section_id = $_POST['section_id'];
		
			// set section_id to return to the current section			
			if (isset($section_id))
			{
				$_SESSION['last_edit_section'] = intval($section_id);
			}
			
			// Create new order object an reorder		
			if($this->oLO->move_up($accordion_id)) 
			{
				// Clean up ordering
				$this->oLO->clean($section_id); 
				
				// save = ok and forward to start screen
			    $this->admin->print_success(
                    $this->language['save_ok'],
                    $this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']
                );
            } else 
			{
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
			}			
			
		}
	}	
	
	
	public function delete_accordion( $iAccordionId = 0 )
	{
		if(isset($_POST['delete_accordion'] ) && $_POST['delete_accordion'] != 0 )
		{
			$accordion_id = intval($_POST['delete_accordion']);
			$section_id = $_POST['section_id'];
		
			// set section_id to return to the current section			
			if(isset($section_id)) 
			{
				$_SESSION['last_edit_section'] = intval($section_id);
			}
			$result = $this->database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_accordion WHERE id = ".$accordion_id." ");	

			if($result == false) {
				die (LEPTON_tools::display($this->database->get_error(),'pre','ui red message'));
			}
			else 
			{
				// Clean up ordering
				$this->oLO->clean($section_id); 
				
				// save = ok and forward to start screen
				$this->admin->print_success(
                    $this->language['delete_ok'],
                    $this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']
                );
			}
		}
	}

	public function open_accordion( $iAccordionId = 0 )
	{
		if(isset($_POST['open_accordion'] ) && $_POST['open_accordion'] != 0 )
		{
			$accordion_id = intval($_POST['open_accordion']);
			$section_id = $_POST['section_id'];
		
			// set section_id to return to the current section			
			if (true === isset($section_id)) 
			{
				$_SESSION['last_edit_section'] = intval($section_id);
			}			

			$open_old = $this->database->get_one("SELECT id FROM ".TABLE_PREFIX."mod_accordion WHERE section_id = ".$section_id." and open = 1 ");
			
			if ($open_old > 0)
			{
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_accordion SET open = 0 WHERE id = ".$open_old." ");	
			}
			
			if( $accordion_id != $open_old) // only inactive gets activated
			{
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_accordion SET open = 1 WHERE id = ".$accordion_id." ");	
			}			

			// save = ok and forward to start screen
			$this->admin->print_success(
                $this->language['save_ok'],
                $this->action.$_POST['page_id']."&leptoken=".$_POST['leptoken']
            );			
		}
	}
}