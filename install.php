<?php

/**
 * @module          Accordion
 * @author          ISB
 * @copyright       2019-2024 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


// install table
$table_fields="
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`page_id` int(11) NOT NULL DEFAULT '-1',
	`section_id` int(11) NOT NULL DEFAULT '-1',
	`title` varchar(256) NOT NULL DEFAULT 'none',
	`content` text,
	`position` int(11) NOT NULL DEFAULT '0',
	`open` int(1) NOT NULL DEFAULT '0',
	`active` int(1) NOT NULL DEFAULT '1',
	PRIMARY KEY ( `id` )
	";
LEPTON_handle::install_table("mod_accordion", $table_fields);

// install droplets
LEPTON_handle::install_droplets('mod_accordion','droplet_display-accordion');
