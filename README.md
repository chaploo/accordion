### Accordion
=========

Displays accordions using [Fomantic Library][3].


#### Requirements

* [LEPTON CMS][1], Version see precheck.php
* include jquery and semantic in your frontend template (use headers.inc.php)
 

#### Installation

* download latest [Accordion.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon create a page or section using accordion module. <br />
For further details please see [readme file][4].


[1]: https://lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com/lepador/modules/accordion.php
[3]: https://fomantic-ui.com
[4]: https://www.internet-service-berlin.de/_documentation/accordion/readme.php

### Image Credits

Module Icon "Filmstrip" by Gerd Altman/Pixabay
