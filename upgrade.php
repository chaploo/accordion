<?php

/**
 * @module          Accordion
 * @author          ISB
 * @copyright       2019-2024 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

// install droplets
LEPTON_handle::install_droplets('mod_accordion','droplet_display-accordion');

$database = LEPTON_database::getInstance();

// add database field open
$fields = [];
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."mod_accordion",
	true,
	$fields,
	false
);
if (!in_array($fields['open'], $fields)) 
{
	$database->simple_query(" ALTER TABLE ".TABLE_PREFIX."mod_accordion ADD open int(1) NOT NULL DEFAULT '0' AFTER position ");
}

LEPTON_handle::delete_obsolete_files('/modules/accordion/classes/accordion_hash.php');
LEPTON_handle::delete_obsolete_directories(['/modules/accordion/unit_tests']);
