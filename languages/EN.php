<?php

/**
 * @module          Accordion
 * @author          ISB
 * @copyright       2019-2024 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file

$MOD_ACCORDION = [
	'action'	    => "Aktion",
	'add'	    	=> "Eintrag hinzufügen",
	'content'	    => "Inhalt",
	'delete_ok'     => "Datensatz erfolgreich gelöscht",
	'duplicate'     => "Kopieren",
	'edit'	        => "Bearbeiten",
	'info'	        => "Addon Info",
	'header1'	    => "ID",
	'header2'	    => "Titel",
	'header3'	    => "Aktiv",
	'header4'	    => "Bearbeiten",
	'header5'	    => "Kopieren",
	'header6'	    => "Löschen",
	'help'	    	=> "Hilfe-Seite",
	'list_header1'	=> "Hinzufügen/Ändern Beitrag",
	'new_order_saved' => "Reihenfolge geändert: ",
	'open'	    	=> "Element geöffnet darstellen",
	'save_ok'	    => "Daten erfolgreich gespeichert",
	'to_delete'	    => "wirklich löschen",
	'want_delete'	=> "Wollen Sie den Datensatz",
];
