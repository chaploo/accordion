<?php

/**
 * @module          Accordion
 * @author          ISB
 * @copyright       2019-2024 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;  
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure file


$oACF = accordion_frontend::getInstance();
$oACF->init_page( $page_id, $section_id);

if(empty($_POST) )  
{
	// marker settings
	$form_values = array(
		'oACF'			=> $oACF,
		'section_id'	=> $section_id,
		'page_id'		=> $page_id,	
		'leptoken'		=> get_leptoken()
	);

	// get the template-engine
	$oTwig = lib_twig_box::getInstance();
	$oTwig->registerModule('accordion');
		
	echo $oTwig->render( 
		"@accordion/view.lte",	//	template-filename
		$form_values				//	template-data
	);
}
