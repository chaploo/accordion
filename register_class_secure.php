<?php

/**
 * @module          Accordion
 * @author          ISB
 * @copyright       2019-2024 ISB
 * @link            https://www.internet-service-berlin.de
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 

$files_to_register = [
	'modify.php',
	'ajax_new_order.php',
	'ajax_set_open_states.php'
];

LEPTON_secure::getInstance()->accessFiles($files_to_register);
